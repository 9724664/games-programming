#pragma once

#include "Menus.h"
#include "Game.h"

using namespace std;

class Zorkish
{
private:
	Menus *_menus;
	Game *_game;

public:
	Zorkish();
	~Zorkish();

	void StartGame();
	void PlayAdventures();
};