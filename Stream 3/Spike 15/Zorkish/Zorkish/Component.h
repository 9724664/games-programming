#pragma once

#include "GameObject.h"

class Component : public GameObject
{
private:
	double _variable;

public:
	Component(string name, string descr, double var);

	double GetVariable();
	void SetVariable(double var);
};