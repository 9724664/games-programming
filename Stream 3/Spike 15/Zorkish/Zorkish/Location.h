#pragma once

#include "Container.h"

class Location : public Container
{
private:
	Location* _northLocation;
	Location* _southLocation;
	Location* _eastLocation;
	Location* _westLocation;

public:
	Location(vector<string> ids, string name, string descr);
	~Location();

	void SetLocation(Location* location, string dir);
	Location* GetLocation(string dir);
};