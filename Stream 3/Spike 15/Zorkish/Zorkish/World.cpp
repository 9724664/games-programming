#include "World.h"

World::World(string world)
{
	_player = new Player("Fred", "null");

	if (world == "Mountain World")
	{
		MountainWorld();
	}
	else if(world == "Water World")
	{
		WaterWorld();
	}
}

World::~World()
{
	delete _player;
}

Player * World::GetPlayer()
{
	return _player;
}

void World::MountainWorld()
{
	Location* House = new Location({"house", "Place"}, "House", "A small wooden house, It only has one room");
	Location* Court = new Location({ "courtyard" }, "Courtyard", "Cleared area around the house");
	Location* Forest = new Location({ "forest", "Dark" }, "Forest", "Dark and Spooky..... BOO!");

	House->SetLocation(Court, "north");

	Court->SetLocation(House, "south");
	Court->SetLocation(Forest, "east");
	Court->SetLocation(Forest, "west");
	Court->SetLocation(Forest, "north");

	Forest->SetLocation(Court, "north");
	Forest->SetLocation(Court, "south");
	Forest->SetLocation(Court, "east");
	Forest->SetLocation(Court, "west");


	Component* Weapon = new Component("Weapon", "Can damage things", 25);
	Component* WeaponPen = new Component("WeaponPen", "over 9000", 9001);
	Component* Health = new Component("Health", "Can be damaged", 100);
	Component* NotMovable = new Component("NotMovable", "Can be damaged", NAN);

	Item *Sword = new Item({ "sword", "Saber" }, "Sword", "Glowing sword of a thousand truths", { Weapon });
	Item *Spoon = new Item({ "spoon", "Utensil" }, "Spoon", "Used for eating... or gauging eyes", {Weapon});
	Item *Pen = new Item({ "pen", "Utensil" }, "Pen", "The pen. Mighter than the sword",{WeaponPen});
	Container *Satchel = new Container({ "satchel" }, "Satchel", "Infinite storage possiblites", {});
	Container *Box = new Container({ "box" }, "Box", "Sturdy Storage device", {Health, NotMovable});

	Satchel->GetInventory()->Put(Spoon);
	_player->GetInventory()->Put(Sword);
	House->GetInventory()->Put(Pen);
	House->GetInventory()->Put(Satchel);
	Court->GetInventory()->Put(Box);

	_player->EnterLocation(House);
}

void World::WaterWorld()
{

}