#pragma once

#include "Command.h"

class TakeCommand : public Command
{
public:
	TakeCommand(vector<string> ids) : Command(ids) {};
	string Execute(World* world, vector<string> input);
	string TakeFrom(World* world, string item, string con, int looklength);
};