#pragma once

#include <vector>
#include <algorithm>
#include <string>

using namespace std;

class IdentifiableObject
{
private:
	vector<string> _identifiers;

public:
	IdentifiableObject(vector<string> idents);
	//virtual ~IdentifiableObject();

	void AddIdentifier(string id);
	bool AreYou(string id);
	string FirstID ();
};