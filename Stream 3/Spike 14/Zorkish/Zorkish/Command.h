#pragma once
#include "IdentifiableObject.h"
#include "World.h"

class Command : public IdentifiableObject
{
public:
	Command(vector<string> ids) : IdentifiableObject(ids) {};
	virtual string Execute(World* world, vector<string> input) = 0;
};