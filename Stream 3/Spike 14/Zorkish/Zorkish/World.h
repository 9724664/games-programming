#pragma once

#include "Container.h"
#include "Player.h"
#include <iostream>


class World
{
private:
	Player* _player;

public:
	World(string world);
	~World();

	Player* GetPlayer();
	void MountainWorld();
	void WaterWorld();
};