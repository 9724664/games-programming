#include "Player.h"


Player::Player(string name, string descr)
	: Container({ "me", "meatsack" }, name, descr, {NULL})
{
	_inventory = new Inventory;
}

Player::~Player()
{
	delete _currentLocation;
	delete _inventory;
}

Location* Player::GetLocation()
{
	return _currentLocation;
}

void Player::EnterLocation(Location* location)
{
	_currentLocation = location;
}

void Player::MoveLocation(string dir)
{
	Location* move = this->GetLocation()->GetLocation(dir);

	if (move != NULL)
		_currentLocation = move;
}