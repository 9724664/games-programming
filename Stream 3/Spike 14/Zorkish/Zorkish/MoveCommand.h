#pragma once

#include "Command.h"

class MoveCommand : public Command
{
public:
	MoveCommand(vector<string> ids) : Command(ids) {};
	string Execute(World* world, vector<string> input);
};