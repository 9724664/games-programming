#include "Messaging.h"

void Messaging::Send(int id, Message message)
{
	int size = _messengers.size() - 1;

	for(int i = 0; i < size; i++)
	{
		int tempID = _messengers[i]->getID();

		if(tempID == id)
		{
			_messengers[i]->Recieve(message); 
		}
	}
}

void Messaging::Post(Message message)
{
	_board.emplace_back(message);
}

vector<Message> Messaging::Read()
{
	return _board;
}

void Messaging::AddMessenger(IMessenger* mess)
{
	_messengers.emplace_back(mess);
}