#pragma once
#include "Message.h"

class IMessenger
{
protected:
	int _id;

public:
	virtual void Recieve(Message message) = 0;

	int getID()
	{
		return _id;
	}
};