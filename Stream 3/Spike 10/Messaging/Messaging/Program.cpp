#include "Messaging.h"
#include "Test.h"

using namespace std;

int main(int argc, char *argv[])
{
	Messaging *messaging = new Messaging();

	Test *t1 = new Test(1);
	Test *t2 = new Test(1);
	Test *t3 = new Test(2);

	messaging->AddMessenger(t1);
	messaging->AddMessenger(t2);
	messaging->AddMessenger(t3);

	Message *m1 = new Message("test", "you got a message");
	Message *m2 = new Message("test", "this should be the last message");

	messaging->Send(1,*m1);
	messaging->Post(*m2);
	vector<Message> messages = messaging->Read();

	cout << messages[0].getMessage() << endl;
	
	cin.get();

	delete m1;
	delete m2;

	delete t1;
	delete t2;
	delete t3;

	delete messaging;
}