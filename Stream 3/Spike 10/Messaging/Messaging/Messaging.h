#pragma once
#include "Message.h"
#include "Messenger.h"
#include <string>
#include <vector>

using namespace std;

class Messaging
{
private:
	vector<IMessenger*> _messengers;
	vector<Message> _board;

public:
	//Messaging();

	void Send(int id, Message message);
	void Post(Message message);
	vector<Message> Read();
	void AddMessenger(IMessenger* mess);
};