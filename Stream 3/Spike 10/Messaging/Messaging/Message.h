#pragma once

#include <string>

using namespace std;

class Message
{
private:
	string _name;
	string _message;
	int _extra;

public:
	Message(string name,string message);
	Message(string name,string message, int extra);

	string getName();
	string getMessage();
	int getExtra();
};