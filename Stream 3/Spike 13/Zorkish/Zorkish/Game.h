#pragma once

#include <iostream>
#include "CommandProcessing.h"

using namespace std;

class Game
{
private:
	CommandProcessing *_comProcess;
public:
	Game();
	~Game();

	int Adventure1();
};