#pragma once

#include "Inventory.h"

class Container : public Item
{
protected:
	Inventory* _inventory;

public:
	Container(vector<string> ids, string name, string descr);
	~Container();

	GameObject* Locate(string id);

	Inventory* GetInventory();
	string LongDescr();
};