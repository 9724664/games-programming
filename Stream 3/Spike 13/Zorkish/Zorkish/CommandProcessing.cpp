#include "CommandProcessing.h"

CommandProcessing::CommandProcessing()
{
	Move = new MoveCommand({ _words });
	Look = new LookCommand({ _words });
	Drop = new DropCommand({ _words });
	Take = new TakeCommand({ _words });
	Put = new PutCommand({ _words });
}

CommandProcessing::~CommandProcessing()
{
	delete Move;
	delete Look;
	delete Drop;
	delete Take;
	delete Put;
}

int CommandProcessing::Execute()
{
	int result = 0;

	if (_words.size() > 0)
	{
		if (_words[0] == "move" || _words[0] == "go" || _words[0] == "head")
			cout << Move->Execute(_world, _words);
		else if (_words[0] == "look")
			cout << Look->Execute(_world, _words);
		else if (_words[0] == "drop" || _words[0] == "remove")
			cout << Drop->Execute(_world, _words);
		else if (_words[0] == "take" || _words[0] == "pickup" || _words[0] == "pick")
			cout << Take->Execute(_world, _words);
		else if (_words[0] == "put" || _words[0] == "place")
			cout << Put->Execute(_world, _words);
		else if (_words[0] == "quit" || _words[0] == "qq" || _words[0] == "q")
		{
			cout << "Exiting game..." << endl;
			result = 1;
		}
		else if (_words[0] == "hiscore" || _words[0] == "highscore" || _words[0] == "score")
		{
			result = 2;
		}
		else if (_words[0] == "help" || _words[0] == "?" || _words[0] == "command" || _words[0] == "commands")
		{
			//Manager.AddState(Help);
		}
		else
			cout << "Invalid command. Type 'help' for command list." << endl;
	}

	cout << endl;

	return result;
}

void CommandProcessing::Read()
{
	cout << _world->GetPlayer()->Name() + ", " << endl;
	cout << "You are in " + _world->GetPlayer()->GetLocation()->Name() << endl;

	cout << "Enter command: " << endl;


	//cin >> _input;
	getline(std::cin, _input);
	cout << endl;

	istringstream buf(_input);
	istream_iterator<string> beg(buf), end;
	vector<string> words(beg, end);

	for (auto &i : words)      
		for (auto &j : i)      
			j = tolower(j);

	_words = words;
}

void CommandProcessing::SetWorld(World* world)
{
	_world = world;
}

vector<string> CommandProcessing::ProcessString(string input)
{
	for (int i = 0; i < input.length(); i++)
	{
		input[i] = tolower(input[i]);
	}

	istringstream buf(input);
	istream_iterator<string> beg(buf), end;
	vector<string> tokens(beg, end);

	return tokens;
}

