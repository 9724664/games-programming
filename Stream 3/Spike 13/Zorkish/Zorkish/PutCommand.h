#pragma once
#include "Command.h"

class PutCommand : public Command
{
public:
	PutCommand(vector<string> ids) : Command(ids) {};
	string Execute(World* world, vector<string> input);
};