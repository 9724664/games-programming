#include "Location.h"

Location::Location(vector<string> ids, string name, string descr)
	: Container(ids, name, descr)
{
	_inventory = new Inventory();

	_northLocation = NULL;
	_southLocation = NULL;
	_eastLocation = NULL;
	_westLocation = NULL;
}

Location::~Location()
{
	delete _northLocation;
	delete _southLocation;
	delete _eastLocation;
	delete _westLocation;
	delete _inventory;
}

void Location::SetLocation(Location* location, string dir)
{
	if(dir == "north")
		_northLocation = location;
	else if (dir == "south")
		_southLocation = location;
	else if (dir == "east")
		_eastLocation = location;
	else if (dir == "west")
		_westLocation = location;
}

Location* Location::GetLocation(string dir)
{
	if(dir == "north")
		return _northLocation;
	else if (dir == "south")
		return _southLocation;
	else if (dir == "east")
		return _eastLocation;
	else if (dir == "west")
		return _westLocation;
}