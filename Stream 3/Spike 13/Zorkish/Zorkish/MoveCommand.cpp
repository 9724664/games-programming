#include "MoveCommand.h"

string MoveCommand::Execute(World * world, vector<string> input)
{
	if (input.size() > 1)
	{
		if (input[1] == "north" || input[1] == "n")
			if (world->GetPlayer()->GetLocation()->GetLocation("north") != NULL)
				world->GetPlayer()->MoveLocation("north");
			else
				return "There is no parth north";
		else if (input[1] == "south" || input[1] == "s")
			if (world->GetPlayer()->GetLocation()->GetLocation("south") != NULL)
				world->GetPlayer()->MoveLocation("south");
			else
				return "There is no parth south";
		else if (input[1] == "east" || input[1] == "e")
			if (world->GetPlayer()->GetLocation()->GetLocation("east") != NULL)
				world->GetPlayer()->MoveLocation("east");
			else
				return "There is no parth east";
		else if (input[1] == "west" || input[1] == "w")
			if (world->GetPlayer()->GetLocation()->GetLocation("west") != NULL)
				world->GetPlayer()->MoveLocation("west");
			else
				return "There is no parth west";
	}
	else
		return "I don't know where to go";
	return "";
}
