#include "DropCommand.h"

string DropCommand::Execute(World* world, vector<string> input)
{
	GameObject* obj = NULL;

	if (input.size() == 2)
	{
		obj = world->GetPlayer()->Locate(input[1]);

		if (obj != NULL)
		{
			Item* item = world->GetPlayer()->GetInventory()->Take(input[1]);
			world->GetPlayer()->GetLocation()->GetInventory()->Put(item);
			return item->Name() + " has been dropped.";
		}
		else
			return "I don't have" + input[1] + ".";
	}
	else
		return "I don't know what to drop.";

	return "Drop error";
}