#include "Game.h"
#include "World.h"

Game::Game()
{
	_comProcess = new CommandProcessing();
}

Game::~Game()
{
	delete _comProcess;
}

int Game::Adventure1()
{
	World* w = new World("Mountain World");
	_comProcess->SetWorld(w);

	bool quit = false;
	int goToAfter = 0;

	while(!quit)
	{
		_comProcess->Read();
		int result = _comProcess->Execute();

		if(result == 1)
		{
			cout << "Your adventure has ended without fame or fortune." << endl;
			goToAfter = 0;
			quit = true;
		}
		else if(result == 2)
		{
			cout << "You have entered the magic word and will now see the �New High Score� screen." << endl;
			goToAfter = 1;
			quit = true;
		}
	}

	return goToAfter;
}

