#pragma once

#include "IdentifiableObject.h"

//using namespace std;

class GameObject : public IdentifiableObject
{
protected:
	string _descr;
	string _name;

public:
	GameObject(vector<string> ids, string name, string descr);

	string Name();
	string Description();
};