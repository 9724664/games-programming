#include "Menus.h"

Menus::Menus()
{
}

int Menus::MainMenu()
{
	string input;
	
	cout << "--------------------------------------------------------" << endl;
	cout << "Welcome to Zorkish Adventures" << endl;
	cout << "1. Select Adventure and Play" << endl;
	cout << "2. Hall Of Fame" << endl;
	cout << "3. Help" << endl;
	cout << "4. About" << endl;
	cout << "5. Quit" << endl;
	cout << "Select 1-5:>";

	cin >> input;
	
	int value;

	bool getInt = StringToInt(input, value);

	if(!getInt)
		value = 0;

	return value;
}
void Menus::AboutMenu()
{
	cout << "Zorkish :: About" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Written by: Eden Beavis (9724664)" << endl;
	cout << "Press ESC or Enter to return to the Main Menu" << endl;

	this->QuitReturn();
}

void Menus::HelpMenu()
{
	cout << "Zorkish :: Help" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "The following commands are supported:" << endl;
	cout << "quit," << endl;
	cout << "hiscore (for testing)" << endl;
	cout << "Press ESC or Enter to return to the Main Menu" << endl;

	this->QuitReturn();
}

int Menus::AdventureMenu()
{
	string input;

	cout << "Zorkish :: Select Adventure" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Choose your adventure:" << endl;
	cout << "1. Mountain World" << endl;
	cout << "2. Water World" << endl;
	cout << "3. Box World" << endl;
	cout << "Select 1-3:>";
	
	cin >> input;
	
	int value;

	bool getInt = StringToInt(input, value);

	if(!getInt)
		value = 0;

	return value;
}

void Menus::HighScoreMenu()
{
	string input;

	cout << "Zorkish :: New High Score" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Congratulations!" << endl;
	cout << "You have made it to the Zorkish Hall Of Fame" << endl;
	cout << "Adventure: [the adventure world played]" << endl;
	cout << "Score: [the players score]" << endl;
	cout << "Moves: [number of moves player made]" << endl;
	cout << "Please type your name and press enter:" << endl;
	cout << ":>";

	bool quit = false;
	
	while(!quit)
	{
		cin >> input;
		quit = true;
	}
}

void Menus::ViewHallOfFameMenu()
{
	cout << "Zorkish :: Hall Of Fame" << endl;
	cout << "--------------------------------------------------------" << endl;
	cout << "Top 10 Zorkish Adventure Champions" << endl;
	cout << "1. Fred, Mountain World, 5000" << endl;
	cout << "2. Mary, Mountain World, 4000" << endl;
	cout << "3. Joe, Water World, 3000" << endl;
	cout << "4. Henry, Mountain World, 2000" << endl;
	cout << "5. Susan, Mountain World, 1000" << endl;
	cout << "6. Alfred, Water World, 900" << endl;
	cout << "7. Clark, Mountain World, 800" << endl;
	cout << "8. Harold, Mountain World, 500" << endl;
	cout << "9. Julie, Water World, 300" << endl;
	cout << "10. Bill, Box World, -5" << endl;
	cout << "Press ESC or Enter to return to the Main Menu" << endl;

	this->QuitReturn();
}

void Menus::QuitReturn()
{
	bool quit = false;

	while(!quit)
	{
		if(kbhit())
		{
			if (GetAsyncKeyState(VK_ESCAPE) || GetAsyncKeyState(VK_RETURN))
				quit = true;
		}
	}
}

bool Menus::StringToInt(const std::string& str, int& result)
{
	std::string::const_iterator i = str.begin();

	if (i == str.end())
		return false;

	bool negative = false;

	if (*i == '-')
	{
		negative = true;
		++i;

		if (i == str.end())
			return false;
	}

	result = 0;

	for (; i != str.end(); ++i)
	{
		if (*i < '0' || *i > '9')
			return false;

		result *= 10;
		result += *i - '0';
	}

	if (negative)
	{
		result = -result;
	}

	return true;
}
