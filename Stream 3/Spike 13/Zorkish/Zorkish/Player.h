#pragma once

#include "Location.h"

class Player : public Container
{
private:
	Location* _currentLocation;

public:
	Player(string name, string descr);
	~Player();

	Location* GetLocation();
	void EnterLocation(Location* location);
	void MoveLocation(string dir);
};