#include "Message.h"

Message::Message(string name,string message)
{
	_name = name;
	_message = message;
}

Message::Message(string name,string message, int extra)
{
	_name = name;
	_message = message;
	_extra = extra;
}

string Message::getName(){ return _name; }
string Message::getMessage(){ return _message; }
int Message::getExtra(){ return _extra;}