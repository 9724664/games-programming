﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    class Swin_Trek
    {
        public static void Main(string[] args)
        {

            Console.WriteLine("Welcome to Swin Trek");
            Console.WriteLine("Please choose a ship name");
            string name = Console.ReadLine();
            // Takes in name of ship
            Console.WriteLine("Please choose write a short description about the ship");
            string desc = Console.ReadLine();
            // Takes in description

            Ship p = new Ship(name, desc, 1, 2);
            // Creates a ship with user specified name and desc
            Container con = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            p.Inventory.Put(it1);
            p.Inventory.Put(it2);
            p.Inventory.Put(con);
            con.Inventory.Put(it3);
            // Item and container creation
            // Items are put into ship and container

            ExamineCommand e = new ExamineCommand(new string[] {"examine"});
            // new examine command

            string option;

            do
            {

                Console.WriteLine("Examine the contents of the ship or type exit to close");
                option = Console.ReadLine();
                string[] examine = option.Split(' ');
                // Splits the string

                if (examine[0] == "examine")
                {

                    Console.WriteLine(e.Execute(p, examine));
                    // if examine is the first word it will examine the string

                }

            } while (option != "exit");
            
        }
    }
}
