﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    interface IHaveInventory
    {

        string Name
        {

            get;
            // Classes that use this interface must have a Name property that is least a readonly 

        }

        GameObject Locate(string id);
        // Classes using this interface must have a locate method

    }
}
