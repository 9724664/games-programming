﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class Coordinate
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        private int _xdim;
        private int _ydim;
        // X and y Dimensions

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public int XCoord
        {

            get { return _xdim; }
            set { _xdim = value; }

        }

        public int YCoord
        {

            get { return _ydim; }
            set { _ydim = value; }
        
        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Coordinate(int x, int y)
        {
        
            _xdim = x;
            _ydim = y;

        }

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public string CoordToString()
        {

            return "x : " + _xdim.ToString() + " y : "+ _ydim.ToString();
            // Retruns the x and y to string

        }

        public bool EqualCoord(int x, int y)
        {

            if (_xdim == x && _ydim == y)
            {

                return true;
                // If _xdim = x and _ydim = y then return true

            }
            else
            {

                return false;

            }

        }

    }
}
