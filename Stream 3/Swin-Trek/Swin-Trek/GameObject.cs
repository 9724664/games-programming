﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public abstract class GameObject : IdentifiableObject
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        protected string _description;
        protected string _name;

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public string Name
        {

            get { return _name; }

        }

        public string ShortDescription
        {

            get { return _name + " " + FirstId; }
            //returns name than a space and it's firstid

        }

        public virtual string FullDescription
        {

            get { return _description; }

        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public GameObject(string[] ids, string name, string desc) :
        base(ids)
        {

            _name = name;
            _description = desc;
            // The local variables will be set to the parameters entered

        }


    }
}
