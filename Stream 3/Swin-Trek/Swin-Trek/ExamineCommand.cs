﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class ExamineCommand : Command
    {
        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public ExamineCommand(string[] ids) :
        base(ids)
        {

            

        }

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public override string Execute(Ship p, string[] text)
        {
            string examine = text[0];

            if (text.Count() <= 1)
            {
                string error = "Examine command requires at least one object";
                return error;
                // If the text inputted is less than two words, return error
            }

            string thing = text[1];
            string container = "me";
            // Thing is equal to the second item in the array
            // Container is equal to the ship or me

            if (examine != "examine")
            {

                return "Error in examine input";
                // If the first word isn't examine, return error

            }
            if (text.Count() == 2)
            {

                container = "me";
                // If there is two items in the array, container will equal me or the ship

            }
            else if (text.Count() == 4)
            {

                container = text[3];
                // If there is 4 items in the array, container will equal the the 4th item in the array

            }


            return ExamineIn(p, thing, container);
            // Examinein returns a string so we can just return that value


        }

        private string ExamineIn(Ship p, string thingId, string containerId)
        {

            GameObject obj = p.Locate(containerId);
            IHaveInventory con = obj as IHaveInventory;
            // Con is declared as an IHaveInventory and it equals obj as an IHaveInventory

            if(obj == null)
            {

                return "I cannot find the " + containerId;
                // If obj returns null, the container can not be found

            }
            else if (con.Locate(thingId) == null)
            {

                return "I cannot find the " + thingId;
                // If the metheod run on con returns null, The thin could not be found

            }
            else
            {

                return con.Locate(thingId).FullDescription;
                // return the description of the item found

            }

        }
    }
}
