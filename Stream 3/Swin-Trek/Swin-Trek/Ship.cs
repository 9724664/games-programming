﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class Ship : SpaceEntity, IHaveInventory
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        private Inventory _inventory;
        // So the ship can carry items
        private Coordinate _coords;

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public Coordinate Coordinates
        {

            get { return _coords; }
            // Returns its coordiantes

        }

        public override string FullDescription
        {

            get { return "The following items are in " + this.Name + " :" + Environment.NewLine + _inventory.ItemList; }
            // Returns the a list of the items in this ship

        }

        public Inventory Inventory
        {

            get { return _inventory; }

        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Ship(string name, string desc, int x, int y) :
        base(new string[] { "me", "inventory"}, name, desc, x, y)
        {

            _inventory = new Inventory();
            _coords = new Coordinate(x, y);
            
        }

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public GameObject Locate(string id)
        {
            if ((AreYou(id) == true) | (id == Name))
            {
                return this;
                // If are you is true or the id is equal to the name of this container
                // return this container
            }
            else if (_inventory.Fetch(id) != null)
            {

                return _inventory.Fetch(id);
                // If fetch is not null return fetch's returned value
            }
            else
            {
                return null;
            }

        }

    }
}
