﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public abstract class Command : IdentifiableObject
    {
        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Command(string[] ids) :
        base(ids)
        { 

        }

        public abstract string Execute(Ship p, string[] text);

    }
}
