﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class IdentifiableObject
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        private List<string> _identifiers = new List<string>();

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public string FirstId
        {

            get
            {

                if (_identifiers[0] == null)
                {

                    return "";
                    //if there is no first id return nothing

                }
                else
                {

                    return _identifiers[0];
                    //return the first item in the list

                }

            }

        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public IdentifiableObject(string[] idents)
        {
            int num;
          
            num = idents.Count();
            for (int i = 0; i < num; i++)
            {
                _identifiers.Add(idents[i].ToLower());
            }

        }
        // Adds all the idents and creates the object
        // Needs to be added one at a time as idents is an array

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public bool AreYou(string id)
        {
            
            if (_identifiers.Contains(id.ToLower()))
            {

                return true;
                // Will return true if the id is in the list of Idents

            }
            else
            {

                return false;
                // Returns false if the ident isnt in the list

            }

        }

        public void AddIdentifier(string id)
        {

            _identifiers.Add(id.ToLower());

        }


    }
}
