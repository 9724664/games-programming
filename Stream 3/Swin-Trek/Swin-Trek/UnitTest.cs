﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Swin_Trek
{
    [TestFixture()]
    public class UnitTest
    {
        
        #region Id_Objects_Tests

        [Test()]

        public void TestCreation()
        {
            IdentifiableObject id = new IdentifiableObject(new string[] { "id1", "id2" });

            Assert.IsNotNull(id, "Id should contains strings");
        }

        [Test()]

        public void TestNotAreYou()
        {
            IdentifiableObject id = new IdentifiableObject(new string[] { "John", "Steve" });

            Assert.IsFalse(id.AreYou("Wilma"), "Testing for false");
        }

        [Test()]

        public void TestCaseSenstive()
        {

            IdentifiableObject id = new IdentifiableObject(new string[] { "John", "Steve" });

            Assert.IsTrue(id.AreYou("JOHN"), "Testing for true");

        }

        [Test()]

        public void TestFirstId()
        {

            IdentifiableObject id = new IdentifiableObject(new string[] { "John", "Steve" });
            string actual;

            actual = id.FirstId;
            Assert.AreEqual("john", actual, "First Id = John");

        }

        [Test()]

        public void TestAddId()
        {

            IdentifiableObject id = new IdentifiableObject(new string[] { "John", "Steve" });

            id.AddIdentifier("Nick");

            Assert.IsTrue(id.AreYou("Nick"), "Testing for true");

        }

        #endregion

        #region Items_Tests

        [Test()]

        public void TestItemIsIdentifiable()
        {
            Item it = new Item(new string[] { "Sword", "Sabre" },"Sabre","Sword of pure light");

            Assert.IsTrue(it.AreYou("Sword"), "Testing for True");
        }

        [Test()]

        public void TestItemShortDesc()
        {
            Item it = new Item(new string[] { "sword", "sabre" }, "sabre", "Sword of pure light");
            string actual;

            actual = it.ShortDescription;
            Assert.AreEqual(("sabre sword"), actual, "Testing to see if short descs prints");
        }

        [Test()]

        public void TestItemLongDesc()
        {
            Item it = new Item(new string[] { "sword", "sabre" }, "sabre", "Sword of pure light");
            string actual;

            actual = it.FullDescription;
            Assert.AreEqual(("Sword of pure light"), actual, "Testing to see if full desc prints");
        }

        #endregion

        #region Inventory_Tests

        [Test()]

        public void TestFindItem()
        {
            Inventory ivent1 = new Inventory();
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ivent1.Put(it1);
            ivent1.Put(it2);
            ivent1.Put(it3);

            Assert.IsTrue(ivent1.HasItem("sock"), "Testing for True");
        }

        [Test()]

        public void TestNoItemsFound()
        {
            Inventory ivent1 = new Inventory();

            Assert.IsFalse(ivent1.HasItem("potato"), "Testing for False");
        }

        [Test()]

        public void TestFetchItem()
        {
            Inventory ivent1 = new Inventory();
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ivent1.Put(it1);
            ivent1.Put(it2);
            ivent1.Put(it3);
            Item actual;

            actual = ivent1.Fetch("sock");
            Assert.AreEqual((it2), actual, "Testing to see item is fetched and remains in inventory");
        }

        [Test()]

        public void TestTakeItem()
        {
            Inventory ivent1 = new Inventory();
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ivent1.Put(it1);
            ivent1.Put(it2);
            ivent1.Put(it3);
            Item actual;

            actual = ivent1.Take("sock");
            Assert.AreEqual((it2), actual, "Testing to see item is taken and not in inventory");
        }

        [Test()]

        public void TestItemList()
        {
            Inventory ivent1 = new Inventory();
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ivent1.Put(it1);
            ivent1.Put(it2);
            ivent1.Put(it3);
            string actual;

            actual = ivent1.ItemList;
            Assert.AreEqual(("sabre sword\r\nsock sock\r\nmobile phone\r\n"), actual, "Testing to see item list is printed");
        }

        #endregion

        #region Ship_Tests

        [Test()]

        public void TestshipIdentifiable()
        {
            Ship ship = new Ship("ship", "My ship", 1, 2);

            Assert.IsTrue(ship.AreYou("me"), "Testing for True");
        }

        [Test()]

        public void TestShipLocatesItems()
        {
            Ship ship = new Ship("ship", "My ship", 1, 2);
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ship.Inventory.Put(it1);
            ship.Inventory.Put(it2);
            ship.Inventory.Put(it3);
            GameObject actual;

            actual = ship.Locate("sabre");
            Assert.AreEqual((it1), actual, "Testing for True");
        }

        [Test()]

        public void TestShipLocatesItself()
        {
            Ship ship = new Ship("ship", "My ship", 1, 2);
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ship.Inventory.Put(it1);
            ship.Inventory.Put(it2);
            ship.Inventory.Put(it3);
            GameObject actual;

            actual = ship.Locate("me");
            Assert.AreEqual((ship), actual, "Testing for True");
        }

        [Test()]

        public void TestShipLocatesNothing()
        {
            Ship ship = new Ship("ship", "My ship", 1, 2);
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ship.Inventory.Put(it1);
            ship.Inventory.Put(it2);
            ship.Inventory.Put(it3);
            GameObject actual;

            actual = ship.Locate("potato");
            Assert.AreEqual((null), actual, "Testing for True");
        }

        [Test()]

        public void TestShipLongDesc()
        {
            Ship ship = new Ship("ship", "My ship", 1, 2);
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            ship.Inventory.Put(it1);
            ship.Inventory.Put(it2);
            ship.Inventory.Put(it3);
            string actual;

            actual = ship.FullDescription;
            Assert.NotNull(actual, "Testing for True");
        }

        #endregion

        #region Container_Tests

        [Test()]

        public void TestContainerLocatesItems()
        {
            Container b1 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(it1);
            b1.Inventory.Put(it2);
            b1.Inventory.Put(it3);
            GameObject actual;

            actual = b1.Locate("sabre");
            Assert.AreEqual((it1), actual, "Testing for True");
        }

        [Test()]

        public void TestContainerLocatesItself()
        {
            Container b1 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(it1);
            b1.Inventory.Put(it2);
            b1.Inventory.Put(it3);
            GameObject actual;

            actual = b1.Locate("bag");
            Assert.AreEqual((b1), actual, "Testing for True");
        }

        [Test()]

        public void TestContainerLocatesNothing()
        {
            Container b1 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(it1);
            b1.Inventory.Put(it2);
            b1.Inventory.Put(it3);
            GameObject actual;

            actual = b1.Locate("SpaceChicken");
            Assert.AreEqual((null), actual, "Testing for True");
        }

        [Test()]

        public void TestContainerLongDesc()
        {
            Container b1 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(it1);
            b1.Inventory.Put(it2);
            b1.Inventory.Put(it3);
            string actual;

            actual = b1.FullDescription;
            Assert.NotNull(actual, "Testing for True");
        }

        [Test()]

        public void TestContainerLocatesContainer()
        {
            Container b1 = new Container(new string[] { "chest", "case" }, "chest", "A brown chest");
            Container b2 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(b2);
            b2.Inventory.Put(it1);
            b2.Inventory.Put(it2);
            b2.Inventory.Put(it3);
            GameObject actual;

            actual = b1.Locate("bag");
            Assert.AreEqual((b2), actual, "Testing for True");
        }

        [Test()]

        public void TestContainerLocatesItemsWithContainer()
        {
            Container b1 = new Container(new string[] { "chest", "case" }, "chest", "A brown chest");
            Container b2 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(b2);
            b1.Inventory.Put(it1);
            b1.Inventory.Put(it2);
            b2.Inventory.Put(it3);
            GameObject actual;

            actual = b1.Locate("sabre");
            Assert.AreEqual((it1), actual, "Testing for True");
        }

        [Test()]

        public void TestContainerNotLocateItemsInOtherContainer()
        {
            Container b1 = new Container(new string[] { "chest", "case" }, "chest", "A brown chest");
            Container b2 = new Container(new string[] { "bag", "case" }, "bag", "A leather bag");
            Item it1 = new Item(new string[] { "Sword", "Sabre" }, "sabre", "Sword of pure light");
            Item it2 = new Item(new string[] { "Sock", "FootWear" }, "sock", "Coloured Foot Wear");
            Item it3 = new Item(new string[] { "Phone", "Mobile" }, "mobile", "Magic device of talking long distances");
            b1.Inventory.Put(b2);
            b1.Inventory.Put(it1);
            b1.Inventory.Put(it2);
            b2.Inventory.Put(it3);
            GameObject actual;

            actual = b1.Locate("phone");
            Assert.AreEqual((null), actual, "Testing for True");
        }

        #endregion

        #region Examine_Command_Tests

        [Test()]

        public void TestExamineMe()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            string[] text = { "examine", "me" };
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("The following items are in ship :\r\n", actual, "Testing to see that the examine command can examine the ship");
        }

        [Test()]

        public void TestExamineGem()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            Item gem = new Item(new string[] { "gem", "crystal" }, "gem", "Shimmering gem");
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            p.Inventory.Put(gem);
            string[] text = { "examine", "gem"};
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("Shimmering gem", actual, "Testing to see that the examine command can examine the gem");
        }

        [Test()]

        public void TestExamineUnk()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            Item gem = new Item(new string[] { "gem", "crystal" }, "gem", "Shimmering gem");
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            string[] text = { "examine", "gem"};
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("I cannot find the gem", actual, "Testing to see that the examine command returns a null result");
        }

        [Test()]

        public void TestExamineGemInMe()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            Item gem = new Item(new string[] { "gem", "crystal" }, "gem", "Shimmering gem");
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            p.Inventory.Put(gem);
            string[] text = { "examine", "gem", "in", "me" };
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("Shimmering gem", actual, "Testing to see that the examine command can examine the gem in ship");
        }

        [Test()]

        public void TestExamineGemInContainer()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            Container b1 = new Container(new string[] { "chest", "case" }, "chest", "A brown chest");
            Item gem = new Item(new string[] { "gem", "crystal" }, "gem", "Shimmering gem");
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            p.Inventory.Put(b1);
            b1.Inventory.Put(gem);
            string[] text = { "examine", "gem", "in", "chest" };
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("Shimmering gem", actual, "Testing to see that the examine command can examine the gem in the container on the ship");
        }

        [Test()]

        public void TestExamineGemInNoContainer()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            Container b1 = new Container(new string[] { "chest", "case" }, "chest", "A brown chest");
            Item gem = new Item(new string[] { "gem", "crystal" }, "gem", "Shimmering gem");
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            string[] text = { "examine", "gem", "in", "chest" };
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("I cannot find the chest", actual, "Testing to see that the examine command returns a null result");
        }

        [Test()]

        public void TestExamineNoGemInContainer()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            Container b1 = new Container(new string[] { "chest", "case" }, "chest", "A brown chest");
            Item gem = new Item(new string[] { "gem", "crystal" }, "gem", "Shimmering gem");
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            p.Inventory.Put(b1);
            string[] text = { "examine", "gem", "in", "chest" };
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("I cannot find the gem", actual, "Testing to see that the examine command returns a null result when there is a container");
        }

        [Test()]

        public void TestInvalidExamine()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            ExamineCommand e = new ExamineCommand(new string[] { "examine", "command" });
            string[] text = { "potato", "gem", "in", "chest" };
            string actual;


            actual = e.Execute(p, text);
            Assert.AreEqual("Error in examine input", actual, "Testing to see that the examine command returns a null result when there is a container");
        }

        #endregion

        #region Examine_Location_tests

        [Test()]

        public void TestCoordinateToString()
        {
            Coordinate c1 = new Coordinate(1, 2);
            string actual;

            actual = c1.CoordToString();
            Assert.AreEqual("x : 1 y : 2", actual, "Testing to see if it prints to string");
        }

        [Test()]

        public void TestTwoCoordinatesEqual()
        {
            Coordinate c1 = new Coordinate(1, 2);
            Coordinate c2 = new Coordinate(1, 2);

            Assert.AreEqual(c2.CoordToString(), c1.CoordToString(), "Testing for True");
        }

        [Test()]

        public void TestTwoCoordinatesNotEqual()
        {
            Coordinate c1 = new Coordinate(1, 2);
            Coordinate c2 = new Coordinate(1, 3);

            Assert.AreNotEqual(c2.CoordToString(), c1.CoordToString(), "Testing for True");
        }

        [Test()]

        public void TestShipCoordinatesToString()
        {
            Ship p = new Ship("ship", "My ship", 1, 2);
            string actual;

            actual = p.Coordinates.CoordToString();
            Assert.AreEqual("x : 1 y : 2", actual, "Testing to see if it prints to string through ship");
        }

        [Test()]

        public void TestSpaceEntityCoordinatesToString()
        {
            SpaceEntity p = new SpaceEntity(new string[] { "rock" }, "ship", "My ship", 1, 2);
            string actual;

            actual = p.Coordinates.CoordToString();
            Assert.AreEqual("x : 1 y : 2", actual, "Testing to see if it prints to string through ship");
        }

        [Test()]

        public void TestSpaceEntityAddedToLocation()
        {
            Location l = new Location(new string[] { "Galaxy" }, "Galaxy", "Space and stars and stuff");
            SpaceEntity p = new SpaceEntity(new string[] { "rock" }, "ship", "My ship", 1, 2);
            l.AddSpaceEntity(p);
            bool actual;

            actual = l.HasSpaceEntity("ship");
            Assert.AreEqual(true, actual, "Testing for true");
        }

        [Test()]

        public void TestTwoSpaceEntityAddedToSameLocation()
        {
            Location l = new Location(new string[] { "Galaxy" }, "Galaxy", "Space and stars and stuff");
            SpaceEntity p1 = new SpaceEntity(new string[] { "rock" }, "spacaadsadas", "My ship", 1, 2);
            SpaceEntity p2 = new SpaceEntity(new string[] { "rock" }, "space2", "My ship", 1, 2);
            l.AddSpaceEntity(p1);
            l.AddSpaceEntity(p2);
            bool actual;

            actual = l.HasSpaceEntity("space2");
            Assert.AreEqual(false, actual, "Testing for false");
        }

        [Test()]

        public void TestSpaceEntityAndShipAddedToSameLocation()
        {
            Location l = new Location(new string[] { "Galaxy" }, "Galaxy", "Space and stars and stuff");
            SpaceEntity p1 = new SpaceEntity(new string[] { "rock" }, "ship", "My ship", 1, 2);
            Ship p2 = new Ship("ship2", "My ship", 1, 2);
            l.AddSpaceEntity(p1);
            l.AddSpaceEntity(p2);
            bool actual;

            actual = l.HasSpaceEntity("ship2");
            Assert.AreEqual(true, actual, "Testing for true");
        }

        [Test()]

        public void TestTwoShipAddedToSameLocation()
        {
            Location l = new Location(new string[] { "Galaxy" }, "Galaxy", "Space and stars and stuff");
            Ship p1 = new Ship("ship", "My ship", 1, 2);
            Ship p2 = new Ship("ship2", "My ship", 1, 2);
            l.AddSpaceEntity(p1);
            l.AddSpaceEntity(p2);
            bool actual;

            actual = l.HasSpaceEntity("ship2");
            Assert.AreEqual(true, actual, "Testing for true");
        }

        #endregion

    }
}
