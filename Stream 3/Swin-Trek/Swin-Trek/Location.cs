﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    class Location : IdentifiableObject
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        private int _xMax = 10;
        private int _yMax = 10;
        // Space entities can only be put on a 10 * 10 grid
        private List<SpaceEntity> _spaceEntites = new List<SpaceEntity>();
        // List of space entites that will be held on the grid

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Location(string[] ids, string name, string desc) :
        base(ids)
        {
 
            

        }

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public bool IsSpaceEntityOnCoord(SpaceEntity spc)
        {
            bool result = false;
            //false is a ship
            //true is a space entity

            foreach (SpaceEntity s in _spaceEntites)
            {
                if ((s.Coordinates == spc.Coordinates) & (s.GetType() != typeof(Ship)))
                {

                    result = true;
                    break;
                    // If is s and spc have the same coordinates and s is not a ship
                    // Return true

                }
                else
                {

                    result = false;
                    // return false if they arent on the same coordinates and it is a ship

                }

            }

            return result;

        }



        public bool HasSpaceEntity(string spc)
        {
            bool answer = false;

            foreach (SpaceEntity s in _spaceEntites)
            {
                if (s.Name == spc)
                {
                    answer = true;
                    break;
                    // If the space entity is in the list
                    // answer will be true
                }
                else
                {
                    answer = false;
                }
            }

            return answer;
            // return answer

        }

        public void AddSpaceEntity(SpaceEntity spc)
        {
            if ((IsSpaceEntityOnCoord(spc) != true) & (spc.Coordinates.XCoord < _xMax) & (spc.Coordinates.YCoord < _yMax))
            {

                _spaceEntites.Add(spc);
                // If there is no space entity on the coordiante and the space entity is within the grid
                // Add it to the list

            }

            else
            {

                Console.WriteLine("Space entity at point");
                // If it fails it returns this

            }

        }

        public void RemoveSpaceEntity(string spc)
        {

            if (HasSpaceEntity(spc) == true)
            {
                foreach (SpaceEntity s in _spaceEntites)
                {
                    if (s.Name == spc)
                    {
                        _spaceEntites.Remove(s);
                        break;
                        // If the list Has the space entity, remove it
                    }
                }
            }

        }

        public string SpcEntitiesAtCoord(int x, int y)
        {

            string result = "The following are at the coordinates :";

            foreach (SpaceEntity s in _spaceEntites)
            {

                if (s.Coordinates.XCoord == x && s.Coordinates.YCoord == y)
                {

                    result += "/n" + s.Name;
                    // If there is a space entity at the point it adds the name to the result

                }

            }
            if (result == "The following are at the coordinates :")
            {

                result += "/n Nothing at coordinates";
                // If there is nothing it returns this

            }

            return result;

        }

        public string ShipAtCoord(int x, int y)
        {

            string result = "The following are at the coordinates :";

            foreach (Ship s in _spaceEntites)
            {

                if (s.Coordinates.XCoord == x && s.Coordinates.YCoord == y)
                {

                    result += "/n" + s.Name;
                    // If there is a ship at the point it adds the name to the result

                }

            }
            if (result == "The following are at the coordinates :")
            {

                result += "/n Nothing at coordinates";
                // If there is nothing it returns this

            }

            return result;

        }

    }
}
