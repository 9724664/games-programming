﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class Inventory
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        private List<Item> _items;
        // List of items that can be stored in the inventory

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public string ItemList
        {
        
            get
            {
                string list = null;
            
                foreach(Item i in _items)
                {
                
                    list += i.Name + " " + i.FirstId + Environment.NewLine;

                }
                // Foreach item in the list, add its name and firstid and then make a new line

                return list;

            }

        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Inventory()
        {

            _items = new List<Item>();

        }

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public bool HasItem(string id)
        {
            bool answer = false;

            foreach( Item i in _items)
            {
                if (i.Name == id)
                {
                    answer = true;
                    break;
                    // If the Item's name is the same as the id, make answer equal to true
                }
                else
                {

                    answer = false;
                    // If the item isnt in the list make answer equal to false

                }
            }

            return answer;

        }

        public void Put(Item itm)
        {

            _items.Add(itm);
            // Add item to the list

        }

        public Item Take(string id)
        {

            Item take = null;
            if (HasItem(id) == true)
            {
                foreach (Item i in _items)
                {
                    if (i.Name == id)
                    {
                        take = i;
                        _items.Remove(i);
                        break;
                        // If HasItems is true then remove the item from the list
                        // Break from the foreach so only one is removed
                    }
                }
            }
            else
            {

                take = null;
                // If HasItem is false return nothing

            }

            return take;

        }

        public Item Fetch(string id)
        { 
        
            Item fetch = null;
            if (HasItem(id) == true)
            {
                foreach (Item i in _items)
                {
                    if (i.Name == id)
                    {
                        fetch = i;
                        break;
                        // If HasItems is true then return the items name
                    }
                }
            }
            else
            {

                fetch = null;
                // If hasitems is false return nothing

            }

            return fetch;

        }

     }
 }


