﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class SpaceEntity : GameObject
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        Coordinate _coords;

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public Coordinate Coordinates
        {

            get { return _coords; }
            // Returns its coordiantes

        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public SpaceEntity(string[] ids, string name, string desc, int x, int y) :
        base(ids, name, desc)
        { 
        
            _coords = new Coordinate(x, y);

        }

    }
}
