﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class Container : Item, IHaveInventory
    {
        //---------------------------------------
        //--Fields--//
        //---------------------------------------

        private Inventory _inventory;
        // Will contain the items in the container

        //---------------------------------------
        //--Properties--//
        //---------------------------------------

        public Inventory Inventory
        {

            get { return _inventory; }

        }

        public override string FullDescription
        {

            get { return "The following items are in " + this.Name + " :" + Environment.NewLine + _inventory.ItemList; }
            // Returns the a list of the items in this container

        }

        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Container(string[] ids, string name, string desc) :
        base(ids, name, desc)
        {

            _inventory = new Inventory();

        }

        //---------------------------------------
        //--Methods--//
        //---------------------------------------

        public GameObject Locate(string id)
        {
            if ((AreYou(id) == true) | (id == Name))
            {
                return this;
                // If are you is true or the id is equal to the name of this container
                // return this container
            }
            else if (_inventory.Fetch(id) != null)
            {

                return _inventory.Fetch(id);
                // If fetch is not null return fetch's returned value

            }
            else
            {
                return null;
            }

        }

    }

}
