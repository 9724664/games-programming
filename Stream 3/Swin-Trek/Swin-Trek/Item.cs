﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Swin_Trek
{
    public class Item : GameObject
    {
        //---------------------------------------
        //--Constructor--//
        //---------------------------------------

        public Item(string[] ids, string name, string desc) :
        base(ids, name, desc)
        {

            _name = name;
            _description = desc;


        }

    }
}
