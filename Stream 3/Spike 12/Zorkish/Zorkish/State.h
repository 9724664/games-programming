#ifndef STATE_H
#define STATE_H
#pragma once
#include <string>


using namespace std;

class StateManager;
class State
{
public:
	virtual void Render() =0;
	virtual void Process(StateManager& Manager) =0;
	virtual string Name() =0;

protected:
	string _name;
	string _option;
};
#endif