#include "SelectState.h"

SelectState::SelectState()
{
	_name = "Game Selection";
	_option = "";

	GameM = new GameState("Mountain");
	GameO = new GameState("eeep");
}

SelectState::~SelectState()
{
	delete GameM;
	delete GameO;
}

void SelectState::Render()
{
	cout << "Select Zorkish game type." << endl;
	cout << "1. Moutain World" << endl;
	cout << "2. Water World." << endl;
	cout << "3. Box World" << endl;
	cout << "4. Return to menu." << endl;
	cout << "Select 1-4" << endl;
	cout << ":>";
}

void SelectState::Process(StateManager& manager)
{
	cin >> _option;

	if (_option == "1")
		manager.AddState(GameM);
	else if (_option == "2")
		manager.AddState(GameO);
	else if (_option == "3")
		manager.AddState(GameO);
	else if (_option == "4")
		manager.RemoveState();
	else
		cout << "Invalid option" << endl;
}

string SelectState::Name()
{
	return _name;
}
