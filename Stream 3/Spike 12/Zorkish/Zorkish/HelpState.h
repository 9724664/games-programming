#pragma once
#include "State.h"
#include "StateManager.h"

class HelpState : virtual public State
{
public:
	HelpState();

	void Render();
	void Process(StateManager& manager);

	string Name();
};