#include "MenuState.h"

MenuState::MenuState()
{
	About = new AboutState();
	Hall = new HallState();
	Help = new HelpState();
	Select = new SelectState();

	_name = "Main Menu";
	_option = "";
}

MenuState::~MenuState()
{
	delete About;
	delete Hall;
	delete Help;
	delete Select;
}

void MenuState::Render()
{
	cout << "Welcome to Zorkish Adventure" << endl;
	cout << "1. Select Adventure and Play." << endl;
	cout << "2. Hall of Fame." << endl;
	cout << "3. Help." << endl;
	cout << "4. About." << endl;
	cout << "5. Quit." << endl;
	cout << "Select 1-5" << endl;
	cout << ":>";
}

void MenuState::Process(StateManager & manager)
{
	cin >> _option;

	if (_option == "1")
		manager.AddState(Select);
	else if (_option == "2")
		manager.AddState(Hall);
	else if (_option == "3")
		manager.AddState(Help);
	else if (_option == "4")
		manager.AddState(About);
	else if (_option == "5")
		manager.RemoveState();
	else
		cout << "Invalid option" << endl;
}

string MenuState::Name()
{
	return _name;
}
