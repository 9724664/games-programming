#pragma once
#include "GameState.h"
#include "StateManager.h"

class SelectState : virtual public State
{
public:
	SelectState();
	~SelectState();

	void Render();
	void Process(StateManager& manager);

	string Name();

private:
	State* GameM;
	State* GameO;
};