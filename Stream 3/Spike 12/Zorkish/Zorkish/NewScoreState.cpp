#include "NewScoreState.h"

NewScoreState::NewScoreState()
{
	_name = "New High Score";
	_option = "";
}

void NewScoreState::Render()
{
	cout << "Congratulations!" << endl;
	cout << "You have made it to the Zorkish Hall Of Fame" << endl;
	cout << "Adventure: [the adventure world played]" << endl;
	cout << "Score: [the players score]" << endl;
	cout << "Moves: [number of moves player made]" << endl;
	cout << "Please type your name and press enter:" << endl;
	cout << ":>";
}

void NewScoreState::Process(StateManager & manager)
{
	bool quit = false;

	while (quit == false)
	{
		if (GetAsyncKeyState(VK_ESCAPE) & 0x8000 || GetAsyncKeyState(VK_BACK) & 0x8000)
		{
			quit = true;
			manager.RemoveState();
			manager.RemoveState();
			manager.RemoveState();
		}
	}
}

string NewScoreState::Name()
{
	return _name;
}
