#pragma once
#include "State.h"
#include "AboutState.h"
#include "HallState.h"
#include "HelpState.h"
#include "SelectState.h"
#include "StateManager.h"

class MenuState : virtual public State
{
public:
	MenuState();
	~MenuState();

	void Render();
	void Process(StateManager& manager);

	string Name();

private:

	State* About;
	State* Hall;
	State* Help;
	State* Select;
};