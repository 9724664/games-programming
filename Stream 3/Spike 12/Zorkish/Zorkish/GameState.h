#pragma once
#include "State.h"
#include "StateManager.h"
#include "NewScoreState.h"
#include <vector>
#include <sstream>
#include <iterator>

class GameState : virtual public State
{
public:
	GameState(string world);
	~GameState();

	void Render();
	void Process(StateManager& manager);

	string Name();

private:
	State* Hall;
	State* Help;
	State* NewScore;

	bool _loaded = false;
};