#include "StateManager.h"

StateManager::StateManager()
{
	_startState = new MenuState();
	_states.push(_startState);
}

void StateManager::Run()
{
	while (_states.empty() != true)
	{
		cout << endl << "Zorkish :: " + _states.top()->Name() << endl << "-------------------------------------------------" << endl;

		_states.top()->Render();
		_states.top()->Process(*this);
	}
}

void StateManager::AddState(State* state)
{
	_states.push(state);
}

void StateManager::RemoveState()
{
	_states.pop();
}
