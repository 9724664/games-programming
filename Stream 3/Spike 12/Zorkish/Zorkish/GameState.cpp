#include "GameState.h"


GameState::GameState(string world)
{
	Hall = new HallState();
	Help = new HelpState();
	NewScore = new NewScoreState();

	_name = "Game";
	_option = "";

	if (world == "Mountain")
	{
		_name = "Mountain World";
	}
	else
	{
		_name = "Some other world";
	}
}

GameState::~GameState()
{
	delete Hall;
	delete Help;
	delete NewScore;
}

void GameState::Render()
{
	if (_loaded == false)
	{
		cout << "Welcome to " + _name << endl;

		string input;
		getline(cin, input);
		cout << endl;

		_loaded = true;
	}

	cout << "You are in " + _name + " starting area." << endl;
	cout << "Enter command: " << endl;
	cout << ":>";
}

void GameState::Process(StateManager& manager)
{
	string input;
	getline(cin, input);
	cout << endl;

	istringstream buf(input);
	istream_iterator<string> beg(buf), end;
	vector<string> words(beg, end);

	for (auto &i : words)   
		for (auto &j : i)      
			j = toupper(j);

	if (words.size() > 0)
	{
		if (words[0] == "QUIT" || words[0] == "QQ" || words[0] == "Q")
		{
			cout << "Exiting game..." << endl;
			manager.RemoveState();
		}
		else if (words[0] == "HISCORE" || words[0] == "HIGHSCORE" || words[0] == "SCORE")
		{
			manager.AddState(NewScore);
			//manager.RemoveState();
		}
		else if (words[0] == "HELP" || words[0] == "?" || words[0] == "COMMAND" || words[0] == "COMMANDS")
		{
			manager.AddState(Help);
		}
		else
			cout << "Invalid input. Type 'help' for valid inputs." << endl;
	}
	else
		cout << "Invalid input. Type 'help' for valid inputs." << endl;

	cout << endl;
}

string GameState::Name()
{
	return _name;
}
