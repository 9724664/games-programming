#include "HelpState.h"

HelpState::HelpState()
{
	_name = "Help";
	_option = "";
}

void HelpState::Render()
{
	cout << "The following commands are supported:" << endl;
	cout << "quit," << endl;
	cout << "hiscore (for testing)" << endl;
	cout << "Press ESC or Enter to return to the Main Menu" << endl;
}

void HelpState::Process(StateManager & manager)
{
	bool quit = false;

	while (quit == false)
	{
		if (GetAsyncKeyState(VK_ESCAPE) & 0x8000 || GetAsyncKeyState(VK_BACK) & 0x8000)
		{
			quit = true;
			manager.RemoveState();
		}
	}
}

string HelpState::Name()
{
	return _name;
}
