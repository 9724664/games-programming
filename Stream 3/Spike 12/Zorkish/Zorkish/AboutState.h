#pragma once
#include "State.h"
#include "StateManager.h"

class AboutState : virtual public State
{
public:
	AboutState();

	void Render();
	void Process(StateManager& manager);

	string Name();
};