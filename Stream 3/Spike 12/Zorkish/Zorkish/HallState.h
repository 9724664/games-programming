#pragma once
#include "State.h"
#include "StateManager.h"

class HallState : virtual public State
{
public:
	HallState();

	void Render();
	void Process(StateManager& manager);

	string Name();
};