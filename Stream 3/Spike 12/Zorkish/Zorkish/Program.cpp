#include "StateManager.h"
#include "State.h"

int main(int argc, char *argv[])
{
	StateManager *m = new StateManager();
	m->Run();

	delete m;

	return 0;
}