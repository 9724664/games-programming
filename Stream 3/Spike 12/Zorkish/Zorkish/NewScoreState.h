#pragma once
#include "State.h"
#include "StateManager.h"

class NewScoreState : virtual public State
{
public:
	NewScoreState();

	void Render();
	void Process(StateManager& manager);

	string Name();
};