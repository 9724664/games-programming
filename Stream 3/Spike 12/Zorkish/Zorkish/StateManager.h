#ifndef STATEMANGER_H
#define STATEMANGER_H

#include "MenuState.h"
#include "State.h"
#include <stack> 
#include <iostream>
#include <windows.h>

#pragma once

using namespace std;

class State;
class StateManager
{
public:
	StateManager();

	void Run();
	void AddState(State* state);
	void RemoveState();

private:
	State* _startState;
	stack <State*> _states;

};
#endif 

