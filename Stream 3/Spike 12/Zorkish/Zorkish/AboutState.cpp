#include "AboutState.h"

AboutState::AboutState()
{
	_name = "About";
	_option = "";
}

void AboutState::Render()
{
	cout << "Written by: Eden Beavis (9724664)" << endl;
	cout << "Press ESC or Enter to return to the Main Menu" << endl;
}

void AboutState::Process(StateManager & manager)
{
	bool quit = false;

	while (quit == false)
	{
		if (GetAsyncKeyState(VK_ESCAPE) & 0x8000 || GetAsyncKeyState(VK_BACK) & 0x8000)
		{
			quit = true;
			manager.RemoveState();
		}
	}
}

string AboutState::Name()
{
	return _name;
}
