#pragma once

#include <iostream>
#include <Windows.h>
#include <conio.h>
#include "CommandProcessing.h"

using namespace std;

class Menus
{
public:
	Menus();

	int MainMenu();
	int AdventureMenu();
	void AboutMenu();
	void HelpMenu();
	void HighScoreMenu();
	void ViewHallOfFameMenu();

	bool StringToInt(const std::string& str, int& result);

	void QuitReturn();
};