#pragma once
#include "Command.h"

class DropCommand : public Command
{
public: 
	DropCommand(vector<string> ids) : Command(ids) {};
	string Execute(World* world, vector<string> input);
};