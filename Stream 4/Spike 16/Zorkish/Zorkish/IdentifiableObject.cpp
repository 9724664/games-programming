#include "IdentifiableObject.h"

IdentifiableObject::IdentifiableObject(vector<string> idents)
{
	for (int i = 0; i < idents.size(); i++)
		_identifiers.push_back(idents[i]);
}

void IdentifiableObject::AddIdentifier(string id)
{
	_identifiers.push_back(id);
}

bool IdentifiableObject::AreYou(string id)
{
	if (std::find(_identifiers.begin(), _identifiers.end(), id) != _identifiers.end())
		return true;
	else
		return false;
}

string IdentifiableObject::FirstID()
{
	if(_identifiers.empty())
		return "";
	else
		return _identifiers[0];
}

