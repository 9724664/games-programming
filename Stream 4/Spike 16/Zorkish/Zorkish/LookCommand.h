#pragma once

#include "Command.h"

class LookCommand : public Command
{
public:
	LookCommand(vector<string> ids) : Command(ids) {};
	string Execute(World* world, vector<string> input);
	string LookAtIn(World* world, string item, string con, int looklength);
};