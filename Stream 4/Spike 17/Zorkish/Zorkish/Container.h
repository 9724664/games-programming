#pragma once

#include "Inventory.h"

class Container : public Item
{
protected:
	Inventory* _inventory;

public:
	Container(vector<string> ids, string name, string descr, vector<Component*> comps);
	~Container();

	GameObject* Locate(string id);

	Inventory* GetInventory();
	string LongDescr();
};