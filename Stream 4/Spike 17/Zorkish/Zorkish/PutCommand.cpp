#include "PutCommand.h"

string PutCommand::Execute(World * world, vector<string> input)
{
	string item = input[1];
	string con = input[3];

	string result = "";

	Container* tempCon = NULL;
	GameObject* obj = NULL;

	if (input.size() == 4)
	{
		tempCon = static_cast<Container*>(world->GetPlayer()->GetLocation()->GetInventory()->Fetch(con));
		if (tempCon == NULL)
			tempCon = static_cast<Container*>(world->GetPlayer()->GetInventory()->Fetch(con));
		obj = world->GetPlayer()->Locate(item);
	}
	if (tempCon != NULL)
	{
		if (obj != NULL)
		{
			tempCon->GetInventory()->Put(world->GetPlayer()->GetInventory()->Take(item));
			result = obj->Name() + " has been placed.";
		}
		else
			return "I cannot find " + item + " in " + con + ".";
	}
	else if (tempCon == NULL)
		return "I cannot find " + con + ".";

	return result;
}
