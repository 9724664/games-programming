#pragma once

#include "GameObject.h"
#include "Component.h"

//using namespace std;

class Item : public GameObject
{
private:
	vector<Component*> _components;

public:
	Item(vector<string> ids, string name, string descr, vector<Component*> comps);

	Component* FindComponent(string id);
};