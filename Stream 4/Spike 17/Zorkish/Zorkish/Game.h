#pragma once

#include <iostream>
#include "CommandProcessing.h"
#include "World.h"
#include "Menus.h"

using namespace std;

class Game
{
private:
	CommandProcessing *_comProcess;
	Menus* _menus;

public:
	Game();
	~Game();

	int Adventure1();
};