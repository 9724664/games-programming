#pragma once

#include "Item.h"

class Inventory
{
private:
	vector<Item*> _items;

public:
	~Inventory();

	void Put(Item *item);
	Item* Take(string id);
	Item* Fetch(string id);

	bool HasItem(string id);
	string ItemList();
};