#include "Item.h"

Item::Item(vector<string> ids, string name, string descr, vector<Component*> comps)
	: GameObject(ids, name, descr)
{
	_components = comps;
}

Component * Item::FindComponent(string id)
{
	for (int i = 0; i < _components.size(); i++)
		if (id == _components[i]->Name())
			return _components[i];

	return NULL;
}
