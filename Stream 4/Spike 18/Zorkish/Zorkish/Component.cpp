#include "Component.h"

Component::Component(string name, string descr, double var)
	: GameObject({"Component"}, name, descr)
{
	_variable = var;
}

double Component::GetVariable()
{
	return _variable;
}

void Component::SetVariable(double var)
{
	_variable = var;
}
