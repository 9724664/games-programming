#include "TakeCommand.h"

string TakeCommand::Execute(World * world, vector<string> input)
{
	string result = "";

	if ((input.size() != 2) && (input.size() != 4))
		return "I don't know what to take.";
	else if (input.size() == 4 && input[2] != "from")
		return "What do you want to take from?";
	else if (input.size() == 2)
		return TakeFrom(world, input[1], input[1], input.size());
	else if (input.size() == 4)
		return TakeFrom(world, input[1], input[3], input.size());

	return result;
}

string TakeCommand::TakeFrom(World * world, string item, string con, int looklength)
{
	string result = "";
	string location = world->GetPlayer()->GetLocation()->Name();
	Container* tempCon = NULL;
	GameObject* obj = NULL;

	std::transform(location.begin(), location.end(), location.begin(), ::tolower);

	if (looklength == 2)
	{
		tempCon = world->GetPlayer()->GetLocation();
		obj = world->GetPlayer()->GetLocation()->GetInventory()->Fetch(item);
	}
	else if (looklength == 4)
	{
		tempCon = static_cast<Container*>(world->GetPlayer()->GetLocation()->GetInventory()->Fetch(con));
		if (tempCon == NULL)
			tempCon = static_cast<Container*>(world->GetPlayer()->GetInventory()->Fetch(con));
		if (tempCon != NULL)
			obj = tempCon->Locate(item);
	}
	if (tempCon != NULL)
	{
		if (obj != NULL)
		{
			Component* comp = static_cast<Item*>(obj)->FindComponent("NotMovable");

			if (comp)
				return obj->Name() + " is not movable";

			world->GetPlayer()->GetInventory()->Put(tempCon->GetInventory()->Take(item));
			result = obj->Name() + " has been taken.";
		}
		else
			return "I cannot find " + item + ".";
	}
	else if (tempCon == NULL)
		return "I cannot find " + con + ".";

	return result;
}
