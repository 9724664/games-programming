#include "GameObject.h"

GameObject::GameObject(vector<string> ids, string name, string descr)
	: IdentifiableObject(ids)
{
	_name = name;
	_descr = descr;
}

string GameObject::Name()
{
	return _name;
}

string GameObject::Description()
{
	return _descr;
}