#pragma once

#include <string>
#include <vector>
#include <sstream>
#include <iterator>
#include <unordered_map>

#include "Command.h"
#include "MoveCommand.h"
#include "LookCommand.h"
#include "DropCommand.h"
#include "TakeCommand.h"
#include "PutCommand.h"

using namespace std;

class CommandProcessing
{
private:
	World* _world;
	vector<string> _words;
	string _input;
	unordered_map <string, Command*> _compare;

	Command* Move;
	Command* Look;
	Command* Drop;
	Command* Take;
	Command* Put;

public:
	CommandProcessing();
	~CommandProcessing();

	int Execute();
	void Read();
	void SetWorld(World* world);

	

	vector<string> ProcessString(string input);
};