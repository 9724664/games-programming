#pragma once
#include "LookCommand.h"

string LookCommand::Execute(World* world, vector<string> input)
{
	string result = "";
	if (input.size() == 1)
		return world->GetPlayer()->GetLocation()->Description() + "\n" + world->GetPlayer()->GetLocation()->GetInventory()->ItemList();
	else if ((input.size() != 3) && (input.size() != 5))
		return "What am I looking at?";
	else if (input[1] != "at")
		return "What do you want to look at?";
	else if (input.size() == 5 && input[3] != "in")
		return "What do you want to look in?";
	else if (input.size() == 3)
		return LookAtIn(world, input[2], input[2], input.size());
	else if (input.size() == 5)
		return LookAtIn(world, input[2], input[4], input.size());
	return result;
}

string LookCommand::LookAtIn(World* world, string item, string con, int looklength)
{
	string result = "";
	string name = world->GetPlayer()->Name();
	Container* tempCon = NULL;
	GameObject* tempItem = NULL;

	std::transform(name.begin(), name.end(), name.begin(), ::tolower);


	if (item == name || item == "me" || item == "inventory")
		return world->GetPlayer()->Description() + "\n" + world->GetPlayer()->GetInventory()->ItemList();
	else if (looklength == 5 && con == "inventory")
		tempCon = world->GetPlayer();
	else if (looklength == 5)
	{
		tempCon = static_cast<Container*>(world->GetPlayer()->GetLocation()->GetInventory()->Fetch(con));
		if (tempCon == NULL)
			tempCon = static_cast<Container*>(world->GetPlayer()->GetInventory()->Fetch(con));
	}
	else if (looklength == 3)
		tempCon = world->GetPlayer()->GetLocation();

	if (tempCon != NULL)
	{
		tempItem = tempCon->Locate(item);
		if (tempItem != NULL)
		{
			result = tempItem->Description();

			Component* comp = static_cast<Item*>(tempItem)->FindComponent("Weapon");

			if (comp)
				result += "\n" + tempItem->Name() + " is a " + comp->Description() + "\nDamage = " + to_string(comp->GetVariable());
		}
		else
			return "I cannot find " + item + " in " + world->GetPlayer()->GetLocation()->Name() + ".";
	}
	else if (tempCon == NULL)
		return "I cannot find " + con + ".";

	return result;
}