#include "Inventory.h"

Inventory::~Inventory()
{
	for(int i = 0; i < _items.size(); i++)
		delete _items[i];
}

void Inventory::Put(Item *item)
{
	_items.push_back(item);
}

Item* Inventory::Take(string id)
{
	Item *result = NULL;

	if(HasItem(id) == true)
	{
		for(int i = 0; i < _items.size(); i++)
		{
			if(_items[i]->AreYou(id))
			{
				result = _items[i];
				_items.erase(_items.begin()+i);
				break;
			}
		}
	}

	return result;
}

Item* Inventory::Fetch(string id)
{
	Item *result = NULL;
	for(int i = 0; i < _items.size(); i++)
	{
		if(_items[i]->AreYou(id))
		{
			result = _items[i];
			break;
		}
	}
	return result;
}

bool Inventory::HasItem(string id)
{
	bool result = false;

	for(int i = 0; i < _items.size(); i++)
	{
		if(_items[i]->AreYou(id))
			result = true;
	}

	return result;
}

string Inventory::ItemList()
{
	string descr;
	descr = "Inventory: \n";
	for(int i = 0; i < _items.size(); i++)
		descr += to_string(i+1) + ". " + _items[i]->Name() + "\n";

	return descr;
}