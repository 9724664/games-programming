#include "Container.h"

Container::Container(vector<string> ids, string name, string descr, vector<Component*> comps)
	: Item(ids, name, descr, comps)
{
	_inventory = new Inventory();
}

Container::~Container()
{
	delete _inventory;
}

GameObject* Container::Locate(string id)
{
	if(AreYou(id) == true || id.compare(Name()) == 0)
		return this;
	else
		return _inventory->Fetch(id);
}

Inventory* Container::GetInventory()
{
	return _inventory;
}

string Container::LongDescr()
{
	string output = "The following items are in" + this->Name() + " :" + " \n" + _inventory->ItemList();
	return output;
}