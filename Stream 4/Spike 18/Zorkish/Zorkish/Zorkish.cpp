#include "Zorkish.h"

Zorkish::Zorkish()
{
	_menus = new Menus();
	_game = new Game();
}

Zorkish::~Zorkish()
{
	delete _menus;
	delete _game;
}

void Zorkish::StartGame()
{
	bool quit = false;

	while(!quit)
	{
		int value = _menus->MainMenu();
		
		switch(value)
		{
			case 1:
				this->PlayAdventures();
				break;
			case 2:
				_menus->ViewHallOfFameMenu();
				break;
			case 3:
				_menus->HelpMenu();
				break;
			case 4:
				_menus->AboutMenu();
				break;
			case 5:
				quit = true;
				break;
			default:
				cout << "Invalid Command" << endl;
				break;
		}

	}
}

void Zorkish::PlayAdventures()
{
	bool quit = false;
	int goToNext = 0;


	while(!quit)
	{
		int value = _menus->AdventureMenu();

		switch(value)
		{
			case 1:
				goToNext =_game->Adventure1();
				quit = true;
				break;
			case 2:
				goToNext =_game->Adventure1();
				quit = true;
				break;
			case 3:
				goToNext =_game->Adventure1();
				quit = true;
				break;
			default:
				cout << "Invalid Command" << endl;
				break;
		}
	}

	switch(goToNext)
	{
		case 1:
			_menus->HighScoreMenu();
			break;
		default:
			break;
	}
}