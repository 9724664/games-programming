#include <SDL.h>
#include <stdio.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>

bool one = false;
bool two = false;
bool three = false;
bool zero = false;

	
SDL_Event e;

//The images
SDL_Window* gWindow = NULL;

//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

Mix_Music *gMusic = NULL;


Mix_Chunk *fart = NULL;
Mix_Chunk *glass = NULL;
Mix_Chunk *gun = NULL;

int main( int argc, char* args[] )
{

	bool quit = false;

	//Start SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	gWindow = SDL_CreateWindow("2DDrawing", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900, 600, SDL_WINDOW_SHOWN);

	gScreenSurface = SDL_GetWindowSurface( gWindow );

	if( Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
    {
        printf( "SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError() );
    }


	gMusic = Mix_LoadMUS( "sounds/guiles.wav" );
	fart = Mix_LoadWAV("sounds/fart.wav");
	gun = Mix_LoadWAV("sounds/gun.wav");
	glass = Mix_LoadWAV("sounds/glass.wav");

	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e))
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_KEYUP)
			{
				const char * key = SDL_GetKeyName(e.key.keysym.sym);
				if (e.key.keysym.sym == SDLK_1)
				{
					Mix_PlayChannel( -1, fart, 0 );
				}
				else if (e.key.keysym.sym == SDLK_2)
				{
					Mix_PlayChannel( -1, gun, 0 );
				}
				else if (e.key.keysym.sym == SDLK_3)
				{
					Mix_PlayChannel( -1, glass, 0 );
				}
				else if (e.key.keysym.sym == SDLK_0)
				{
					if( Mix_PlayingMusic() == 0 )
                    {
                        //Play the music
                        Mix_PlayMusic( gMusic, -1 );
                    }
                    //If music is being played
                    else
                    {
                        //If the music is paused
                        if( Mix_PausedMusic() == 1 )
                        {
                            //Resume the music
                            Mix_ResumeMusic();
                        }
                        //If the music is playing
                        else
                        {
                            //Pause the music
                            Mix_PauseMusic();
                        }
                    }
				}
			}
			if( Mix_PlayingMusic() == 0 )
            {
                //Play the music
                Mix_PlayMusic( gMusic, -1 );
            }
		}

        SDL_UpdateWindowSurface( gWindow );
	}

	//Quit SDL
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	SDL_Quit();

	return 0;
}

