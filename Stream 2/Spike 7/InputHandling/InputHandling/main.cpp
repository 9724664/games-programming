#include <SDL.h>
#include <stdio.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>

bool one = false;
bool two = false;
bool three = false;
bool four = false;
bool five = false;
bool six = false;
bool seven = false;
bool eight = false;
bool nine = false;
bool zero = false;
bool Q = false;

void print()
{
	printf("[");

	if (one)
		printf("X");
	else
		printf("-");
	if (two)
		printf("X");
	else
		printf("-");
	if (three)
		printf("X");
	else
		printf("-");
	if (four)
		printf("X");
	else
		printf("-");
	if (five)
		printf("X");
	else
		printf("-");
	if (six)
		printf("X");
	else
		printf("-");
	if (seven)
		printf("X");
	else
		printf("-");
	if (eight)
		printf("X");
	else
		printf("-");
	if (nine)
		printf("X");
	else
		printf("-");
	if (zero)
		printf("X");
	else
		printf("-");

	printf("]");

	if (Q)
		printf(" --- 'Q' is DOWN");
	else
		printf(" --- 'Q': is UP");

	printf("\n");
}

int main( int argc, char* args[] )
{

	bool quit = false;

	bool bgEnabled;
	
	SDL_Event e;

	//The images
	SDL_Window* window;

	//Start SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	
	//char* key;

	window = SDL_CreateWindow("Input Handling", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 100, 100, SDL_WINDOW_SHOWN);

	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e))
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			
			if (e.type == SDL_KEYDOWN)
			{
				const char * key = SDL_GetKeyName(e.key.keysym.sym);
				if (e.key.keysym.sym == SDLK_1)
					one = true;
				else if (e.key.keysym.sym == SDLK_2)
					two = true;
				else if (e.key.keysym.sym == SDLK_3)
					three = true;
				else if (e.key.keysym.sym == SDLK_4)
					four = true;
				else if (e.key.keysym.sym == SDLK_5)
					five = true;
				else if (e.key.keysym.sym == SDLK_6)
					six = true;
				else if (e.key.keysym.sym == SDLK_7)
					seven = true;
				else if (e.key.keysym.sym == SDLK_8)
					eight = true;
				else if (e.key.keysym.sym == SDLK_9)
					nine = true;
				else if (e.key.keysym.sym == SDLK_0)
					zero = true;
				else if (e.key.keysym.sym == SDLK_q)
					Q = true;
				

				print();

			}
			else if (e.type == SDL_KEYUP)
			{
				const char * key = SDL_GetKeyName(e.key.keysym.sym);
				if (e.key.keysym.sym == SDLK_1)
					one = false;
				else if (e.key.keysym.sym == SDLK_2)
					two = false;
				else if (e.key.keysym.sym == SDLK_3)
					three = false;
				else if (e.key.keysym.sym == SDLK_4)
					four = false;
				else if (e.key.keysym.sym == SDLK_5)
					five = false;
				else if (e.key.keysym.sym == SDLK_6)
					six = false;
				else if (e.key.keysym.sym == SDLK_7)
					seven = false;
				else if (e.key.keysym.sym == SDLK_8)
					eight = false;
				else if (e.key.keysym.sym == SDLK_9)
					nine = false;
				else if (e.key.keysym.sym == SDLK_0)
					zero = false;
				else if (e.key.keysym.sym == SDLK_q)
					Q = false;

				print();
			}
		}

		//print();
	}

	//Quit SDL
	SDL_DestroyWindow(window);
	window = NULL;
	SDL_Quit();

	return 0;
}

