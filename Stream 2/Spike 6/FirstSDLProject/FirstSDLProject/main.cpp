#include <SDL.h>
#include <stdio.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
//#include "SDL/SDL.h"

const int SCREEN_WIDTH = 600;
const int SCREEN_HEIGHT = 800;

int main( int argc, char* args[] )
{
	SDL_Window* window = NULL;
	SDL_Surface* screenSurface = NULL;

	if(SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	}
	 else
    {
        window = SDL_CreateWindow( "MyFirstProject", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN );
        if( window == NULL )
        {
            printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
        }
		else
        {
            screenSurface = SDL_GetWindowSurface( window );
            SDL_FillRect( screenSurface, NULL, SDL_MapRGB( screenSurface->format, 0xFF, 0xFF, 0xFF ) );
            SDL_UpdateWindowSurface( window );
           
			bool quit = false;
			SDL_Event e;
			while(!quit)
			{
				while(SDL_PollEvent(&e) != 0)
				{
					if(e.type == SDL_QUIT)
						quit = true;
				}
                SDL_UpdateWindowSurface( window );
			}
        }
	}
    SDL_DestroyWindow( window );
    SDL_Quit();

    return 0;    
}