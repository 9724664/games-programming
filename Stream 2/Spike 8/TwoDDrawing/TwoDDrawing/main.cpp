#include <SDL.h>
#include <stdio.h>
#include <SDL_mixer.h>
#include <SDL_image.h>
#include <stdio.h>
#include <string>
#include <iostream>

bool one = false;
bool two = false;
bool three = false;
bool zero = false;

	
SDL_Event e;

//The images
SDL_Window* gWindow = NULL;

SDL_Surface *background;
SDL_Surface *tile1;
SDL_Surface *tile2;
SDL_Surface *tile3;
SDL_Surface *flat;

SDL_Rect offset1;
SDL_Rect offset2;
SDL_Rect offset3;


//The surface contained by the window
SDL_Surface* gScreenSurface = NULL;

void UpdateSurfaces()
{
	SDL_BlitSurface( flat, NULL, gScreenSurface, NULL );

	if (zero == true)
	{
		 SDL_BlitSurface( background, NULL, gScreenSurface, NULL );
	}
	if (one == true)
	{
		 SDL_BlitSurface( tile1, NULL, gScreenSurface, &offset1 );
	}
	if (two == true)
	{
		 SDL_BlitSurface( tile2, NULL, gScreenSurface, &offset2 );
	}
	if (three == true)
	{
		 SDL_BlitSurface( tile3, NULL, gScreenSurface, &offset3 );
	}
}

int main( int argc, char* args[] )
{

	bool quit = false;

	//Start SDL
	SDL_Init(SDL_INIT_EVERYTHING);

	gWindow = SDL_CreateWindow("2DDrawing", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 900, 600, SDL_WINDOW_SHOWN);

	gScreenSurface = SDL_GetWindowSurface( gWindow );

	background = SDL_LoadBMP("images/Background.bmp");
	tile1 = SDL_LoadBMP("images/Tile1.bmp");
	tile2 = SDL_LoadBMP("images/Tile2.bmp");
	tile3 = SDL_LoadBMP("images/Tile3.bmp");
	flat = SDL_LoadBMP("images/Flat.bmp");

	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e))
		{
			//User requests quit
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
			else if (e.type == SDL_KEYUP)
			{
				const char * key = SDL_GetKeyName(e.key.keysym.sym);
				if (e.key.keysym.sym == SDLK_1)
				{
					if (one == false)
					{
						one = true;
						offset1.x = rand() % 600;
						offset1.y = rand() % 600;
					}
					else
						one = false;
				}
				else if (e.key.keysym.sym == SDLK_2)
				{
					if (two == false)
					{
						two = true;
						offset2.x = rand() % 600;
						offset2.y = rand() % 600;
					}
					else
						two = false;
				}
				else if (e.key.keysym.sym == SDLK_3)
				{
					if (three == false)
					{
						three = true;
						offset3.x = rand() % 600;
						offset3.y = rand() % 600;
					}
					else
						three = false;
				}
				else if (e.key.keysym.sym == SDLK_0)
				{
					if (zero == false)
						zero = true;
					else
						zero = false;
				}

				UpdateSurfaces();
			}
		}

        SDL_UpdateWindowSurface( gWindow );
	}

	//Quit SDL
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;
	SDL_Quit();

	return 0;
}

