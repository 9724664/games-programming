#pragma once

#include <iostream>
#include <string>

class IO
{
private:
	int _posX;
	int _posY;

public:
	IO();

	std::string PlayerMove(std::string move);

	void UpdateGrid(int x, int y, int oldx, int oldy);

	void PrintGrid();
};