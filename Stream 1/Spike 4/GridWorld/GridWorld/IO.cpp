#include "IO.h"

using namespace std;

char _grid[8][8] =	{	{'#','#','#','#','#','#','#','#'},
						{'#','G',' ','D','#','D',' ','#'},
						{'#',' ',' ',' ','#',' ',' ','#'},
						{'#','#','#',' ','#',' ','D','#'},
						{'#',' ',' ',' ','#',' ',' ','#'},
						{'#',' ','#','#','#','#',' ','#'},
						{'#',' ',' ',' ',' ',' ',' ','#'},
						{'#','#','S','#','#','#','#','#'}	};

const char _baseGrid[8][8] =	{	{'#','#','#','#','#','#','#','#'},
									{'#','G',' ','D','#','D',' ','#'},
									{'#',' ',' ',' ','#',' ',' ','#'},
									{'#','#','#',' ','#',' ','D','#'},
									{'#',' ',' ',' ','#',' ',' ','#'},
									{'#',' ','#','#','#','#',' ','#'},
									{'#',' ',' ',' ',' ',' ',' ','#'},
									{'#','#','S','#','#','#','#','#'}	};
IO::IO()
{
	_posX = 7;
	_posY = 2;
}

std::string IO::PlayerMove(string move)
{
	std::string result;

	if(move.compare("N") == 0)
	{
		if(_grid[_posX - 1][_posY] == '#')
		{
			result = "Invalid Move";
		}
		else if (_grid[_posX - 1][_posY] == 'D')
		{
			result = "You have died. Game restarting";
			Reset();
		}
		else if (_grid[_posX - 1][_posY] == 'G')
		{
			result = "You have won! Game restarting";
			Reset();
		}
		else
		{
			_posX = _posX - 1;
			UpdateGrid(_posX, _posY, _posX + 1, _posY);
		}

	}
	else if(move.compare("S") == 0)
	{
		if(_grid[_posX + 1][_posY] == '#' || _grid[_posX + 1][_posY] == 'S' || _posX == 7)
		{
			result = "Invalid Move";
		}
		else if (_grid[_posX + 1][_posY] == 'D')
		{
			result = "You have died. Game restarting";
			Reset();
		}
		else if (_grid[_posX + 1][_posY] == 'G')
		{
			result = "You have won! Game restarting";
			Reset();
		}
		else
		{
			_posX = _posX + 1;
			UpdateGrid(_posX, _posY, _posX - 1, _posY);
		}
	}
	else if(move.compare("E") == 0)
	{
		if(_grid[_posX][_posY + 1] == '#')
		{
			result = "Invalid Move";
		}
		else if (_grid[_posX][_posY + 1] == 'D')
		{
			result = "You have died. Game restarting";
			Reset();
		}
		else if (_grid[_posX][_posY + 1] == 'G')
		{
			result = "You have won! Game restarting";
			Reset();
		}
		else
		{
			_posY = _posY + 1;
			UpdateGrid(_posX, _posY, _posX, _posY - 1);
		}
	}
	else if(move.compare("W") == 0)
	{
		if(_grid[_posX][_posY - 1] == '#')
		{
			result = "Invalid Move";
		}
		else if (_grid[_posX][_posY - 1] == 'D')
		{
			result = "You have died. Game restarting";
			Reset();
		}
		else if (_grid[_posX][_posY - 1] == 'G')
		{
			result = "You have won! Game restarting";
			Reset();
		}
		else
		{
			_posY = _posY - 1;
			UpdateGrid(_posX, _posY, _posX, _posY + 1);
		}
	}
	else if(move.compare("Q") == 0)
	{
		return result;
	}
	else if(move.compare("R") == 0)
	{
		Reset();
		result = "Game Reset";
	}
	else
	{
		result = "Invalid Command, please input N, S, E, W, R or Q";
	}

	return result;
}

void IO::UpdateGrid(int x, int y, int oldx, int oldy)
{
	if (_grid[oldx][oldy] =='S')
	{
		_grid[oldx][oldy] = '#';
	}
	else
	{
		_grid[oldx][oldy] = ' ';
	}

	_grid[x][y] = 'P';
	
}

void IO::Reset()
{
	_posX = 7;
	_posY = 2;

	for (int i = 0; i < sizeof(_grid[0]); i++)
		copy(begin(_baseGrid[i]), end(_baseGrid[i]), begin(_grid[i]));
}

void IO::PrintGrid()
{
	COORD coord;
	coord.X = 0;
	coord.Y = 4;
	SetConsoleCursorPosition( GetStdHandle( STD_OUTPUT_HANDLE ), coord );

	for(int i = 0; i < 8; i++)
	{
		for (int j = 0; j < sizeof(_grid[0]); j++)
		{
			if( j == sizeof(_grid[0]) - 1)
			{
				cout << _grid[i][j] << endl;
			}
			else
			{
				cout << _grid[i][j];
			}
		}
	}
}