#include <iostream>
#include <string>
#include <conio.h>
#include "IO.h"

using namespace std;

int main(int argc, char* argv[])
{
	IO *game = new IO();
	
	cout << "---------------Welcome to GridWorld!-------------" << endl;
	cout << "Legend: G(oal), S(tart), D(eath), P(layer), #(Wall)" << endl;
	cout << "Controls: N(orth), S(outh), E(ast), W(est), R(eset), Q(uit)" << endl;
	cout << "-------------------Make a move!------------------" << endl;
	
	string input = "null";
	string output = "null";
	//bool in_avail;

	game->PrintGrid();

	while(input.compare("Q") != 0)
	{
		if(kbhit())
		{
			cout << "                                                           ";
			cout << "\r";
			std::cin >> input;

			for (int i = 0; i < input.length(); i++)
			{
				input[i] = toupper(input[i]);
			}

			output = game->PlayerMove(input);

			cout << "                                                           ";
			cout << "\r" + output << endl;
		}

		game->PrintGrid();
	}

	delete game;

	return 0;
}